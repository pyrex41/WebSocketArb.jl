module WebSocketArb



include("SocketBase.jl")
include("exchanges/Coinbase.jl")
include("exchanges/Kraken.jl")
include("exchanges/BinanceUS.jl")
include("exchanges/Bittrex.jl")
include("exchanges/SFOX.jl")

end
