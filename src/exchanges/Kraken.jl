module Kraken

using WebSockets, JSON, Actors
using ArbUtils.OrderUtils, Currencies
using ..SocketBase

const symbol = :kraken

# handle non-standard symbols
function kraken_get_pair(pair)
    p = get_pair(pair, "/")
    map(p) do s
        s == :XBT ? :BTC : s
    end
end


pair_to_string(pair) = map(Symbol,pair) |> pair_to_string
pair_to_string(pair::Tuple{Symbol,Symbol}) = string(pair[1], "/", pair[2]) |> uppercase
function subscribe_msg(pairs; level = :ticker)
    level = level == :ticker ? :spread : level
    Dict(
        :event => :subscribe,
        :pair => map(pair_to_string, pairs),
        :subscription => Dict(
            :name => level
        )
    )
end

launch(pairs_dict::Dict, updater::Link; kwargs...) = launch_genaric(config, pairs_dict, updater; kwargs...)

config(pairs::Dict, out_channel::Destination; kwargs...) = config(pairs[:kraken]...; out_channel=out_channel)
config(pairs...; out_channel::Destination) = WebSocketConfig(
    feed = :kraken,
    subscribe_msg = subscribe_msg(pairs; level = :spread),
    url = "wss://beta-ws.kraken.com",
    out_channel = out_channel,
    setup_func = setup_handler,
)


function setup_handler(chan::Destination)

    # this closure here can be used to retain state between updates if need

    handler(resp) = begin
        @warn "This is a default response; something isn't plumbed right"
        @show resp
    end
    function handler(resp::Dict)
        try
            event_type = resp["event"]
            if event_type in ("subscriptionStatus", "systemStatus")
                @info "This is a subscription message:"
                @show resp
            elseif event_type == "heartbeat"
                @debug resp
            else
                @warn "Unexected response"
                @show resp
            end
        catch x
            @warn x
            @show resp
        end
    end
    function handler(resp::Array)
        try
            rtype = resp[3]
            if rtype == "spread"
                tick = handle_spread(resp)
                put!(chan, tick)
            else
                @warn "unexpected message"
                @show resp
            end
        catch x
            @warn x
            @show resp
        end
    end
    handler
end

function handle_spread(jd::Vector) # very similar to what CoinbasePro calls a ticker
    tn = time()
    _, vec, _, raw_pair = jd
    b, a, timestamp, bv, av = map(vec) do i
        parse(Float64,i)
    end
    pair = kraken_get_pair(raw_pair)  # handle :XBT
    Tick(
        symbol,
        pair,
        b,
        bv,
        a,
        av,
        timestamp,
        tn,
        [:bid, :ask]
    )
end

end # end module
