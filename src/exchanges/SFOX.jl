module SFOX

using WebSockets, JSON, Actors
using ArbUtils.OrderUtils, Currencies
using ..SocketBase

const symbol = :sfox

const url = "wss://ws.sfox.com/ws"

launch(pairs_dict::Dict, updater::Link; netprice=true, kwargs...) = launch_genaric(config, pairs_dict, updater; netprice=netprice, kwargs...)

config(pairs::Dict, out_channel::Destination; netprice=true, kwargs...) = config(pairs[:sfox]...; out_channel = out_channel, netprice=netprice)
config(pairs::Tuple{Symbol, Symbol}...; out_channel::Destination, netprice::Bool) = WebSocketConfig(
    feed = :sfox,
    subscribe_msg = subscribe_msg(pairs; netprice=netprice),
    url = url,
    out_channel = out_channel,
    setup_func = presetup_handler(pairs)
)


format(pp::Tuple{Symbol,Symbol}...; booktype::String) = map(pp) do p
    string("orderbook.", booktype, ".", p[1],p[2]) |> lowercase
end
shortformat(p::Tuple{Symbol,Symbol}) = string(p[1],p[2]) |> lowercase

function subscribe_msg(pairs; netprice::Bool)
    booktype = netprice ? "net" : "sfox"
    Dict(
        :type => :subscribe,
        :feeds => format(pairs...; booktype=booktype)
    )
end

function presetup_handler(pairs)

    p_dict = Dict(shortformat(p)=>p for p in pairs)
    pair_lookup(s) = p_dict[s]

    function setup_handler(chanlink::Destination)

        sequence = 1

        handler(resp) = begin
            @warn "This is a defult response"
            @show resp
        end
        function handler(resp::Dict)
            try
                #=
                if resp["sequence"] ≠ sequence
                    @warn "Missed a message somehow"
                    sequence = resp["sequence"]
                else
                    sequence += 1
                end
                =#
                kk = keys(resp)
                if "recipient" in kk
                    timestamp = resp["timestamp"] * 10^-9
                    pair = pair_lookup(resp["payload"]["pair"])
                    tick = handle_payload(resp["payload"], timestamp, pair)
                    put!(chanlink, tick)
                elseif "action" in kk
                    @info "This is a subscription / action response"
                    @show resp
                else
                    @warn "Unexected response"
                    @show resp
                end
            catch x
                @warn x
                @show resp
            end
        end
    end
    setup_handler
end

function handle_payload(payload::Dict, timestamp::Float64, p::Tuple{Symbol,Symbol})
    tn = time()
    b, bv, bm = payload["bids"][1]
    a, av, am = payload["asks"][1]
    Tick(
        :sfox,
        p,
        b,
        bv,
        a,
        av,
        timestamp,
        tn,
        [:bid, bm, :ask, am]
    )
end


end # end module
