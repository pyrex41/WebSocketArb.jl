module Bittrex

using WebSockets, JSON, HTTP, TranscodingStreams, CodecZlib, Base64, Actors
using ArbUtils.OrderUtils, Currencies
using ..SocketBase

const symbol = :bittrex

format(pairs) = map(pairs) do pair
    string("ticker_", pair[1], "-", pair[2])
end

# manage SignalR connection
base_query = Dict(
    :connectionData => HTTP.escapeuri(JSON.json([Dict("name" => "c3")])),
    :clientProtocol => "1.5"
)
host = "socket-v3.bittrex.com"

function gettoken()
    uri = HTTP.URI(scheme="https", host=host, path="/signalr/negotiate", query=Dict("connectionProtocol"=>1.5))
    uri = string(uri, "?connectionData=", HTTP.escapeuri(JSON.json([Dict("name" => "c3")])))  # this is janky, but seems like it's necessary?
    r = HTTP.get(uri)
    jr = r.body |> String |> JSON.parse
    jr["ConnectionToken"]
end

function wuri()
    token = gettoken()
    q = copy(base_query)
    q[:connectionToken] = token |> HTTP.escapeuri
    q[:transport] = "webSockets"
    ws = string("wss://",host,"/signalr/connect?")
    for (k,v) in q
        ws = string(ws, k, "=", v, "&")
    end
    ws[1:end-1]
end

function subscribe_msg(pairs)
    i = 1
    Dict(
        :A => [format(pairs)],
        :H => :c3,
        :I => i,
        :M => :Subscribe
    )
end

launch(pairs_dict::Dict, updater::Link; kwargs...) = launch_genaric(config, pairs_dict, updater; kwargs...)

config(pairs::Dict, out_channel::Destination; kwargs...) = config(pairs[:bittrex]...; out_channel=out_channel)
config(pairs...; out_channel::Destination) = WebSocketConfig(
    feed = :bittrex,
    subscribe_msg = subscribe_msg(pairs),
    url = wuri(),
    out_channel = out_channel,
    setup_func = setup_handler
)

function setup_handler(chan::Destination)

    # if we need to reference previous state, we do that here

    handler(resp) = begin
        @warn "This is a default response for Bittrex; something isn't plumbed right"
        @show resp
    end
    function handler(resp::Dict)
        try
            rr = get(resp, "M", [])
            if length(rr) > 0
                for update in rr
                    if update["M"] == "ticker"
                        tick = handle_tick(update["A"])
                        put!(chan, tick)
                    else
                        @warn "Unexpected message"
                        @show resp
                    end
                end
            end
        catch x
            @warn x
            @show resp
        end
    end
    handler
end

function handle_tick(p)
    tn = time()
    resp = transcode(DeflateDecompressor, base64decode(p[1])) |> String |> JSON.parse
    pair = get_pair(resp["symbol"])
    b = parse(Float64, resp["bidRate"])
    a = parse(Float64, resp["askRate"])
    Tick(
        symbol,
        pair,
        b,
        0.0,
        a,
        0.0,
        tn,
        tn,
        [:bid, :ask]
    )
end

end # end of module
