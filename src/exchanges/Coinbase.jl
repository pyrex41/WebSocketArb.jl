module Coinbase

using WebSockets, JSON, Actors
using ArbUtils.OrderUtils, Currencies
using ..SocketBase

const symbol = :coinbasepro

function setup_handler(chan::Destination)

    tick_holder = Dict{Tuple, Tick}()

    handler(resp) = begin
        @warn "This is a default response; something isn't plumbed right"
        @show resp
    end

    function handler(jd::Dict)
        try
            rtype = jd["type"]
            if rtype in ("subscription", "subscriptions")
                @info "This is a subscription message"
                @show jd
            else
                pair = get_pair(jd["product_id"])
                if rtype == "snapshot"
                    tick = handle_snapshot(jd)
                    tick_holder[tick.pair] = tick
                    put!(chan, tick_holder[pair])
                elseif rtype == "l2update"
                    handle_l2update!(jd, tick_holder)
                    put!(chan, tick_holder[pair])
                elseif rtype == "ticker"
                    tick = handle_ticker(jd)
                    put!(chan, tick)
                else
                    @warn "unexected response"
                    @show jd
                end
            end
        catch x
            @warn x
            @show jd
        end
    end
end

function handle_ticker(jd::Dict)
    pair = get_pair(jd["product_id"])
    timestamp = parse_time(jd["time"]) |> float
    tn = time()
    b = parse(Float64, jd["best_bid"])
    a = parse(Float64, jd["best_ask"])
    Tick(
        :coinbasepro,
        pair,
        b,
        0.0,
        a,
        0.0,
        timestamp,
        tn,
        [:bid, :ask]
    )
end

function handle_snapshot(jd::Dict)
    tn = time()
    pair = get_pair(jd["product_id"])
    inline_1(v::Vector) = (parse(Float64,v[1]), parse(Float64,v[2]))
    b,bv = jd["bids"][1] |> inline_1
    a,av = jd["asks"][1] |> inline_1
    Tick(
        :coinbasepro,
        pair,
        b,
        bv,
        a,
        av,
        tn,
        tn,
        [:bid, :ask]
    )
end


function handle_l2update!(jd::Dict, tick_holder::Dict{Tuple, Tick})
    pair = get_pair(jd["product_id"])
    tick = tick_holder[pair]
    timestamp = parse_time(jd["time"]) |> float
    time_received = time()
    b,bv = (tick.bid, tick.bid_volume)
    a,av = (tick.ask, tick.ask_volume)
    flags = []
    map(jd["changes"]) do row
        lab, pp, vv = row
        p = parse(Float64, pp)
        v = parse(Float64, vv)
        if lab =="buy"
            if p ≥ b
                b, bv = p, v
                push!(flags, :bid)
            end
        else
            @assert lab == "sell"
            if p ≤ a
                a, av = p, v
                push!(flags, :ask)
            end
        end
    end
    new_tick = Tick(
        :coinbasepro,
        pair,
        b,
        bv,
        a,
        av,
        timestamp,
        time_received,
        union(flags)
    )
    tick_holder[pair] = new_tick
end

pair_to_string(pair) = pair_to_string(map(Symbol, pair))
pair_to_string(pair::Tuple{Symbol, Symbol}) = string(pair[1],"-", pair[2]) |> uppercase
subscribe_msg(pairs; level = :ticker)= Dict(
    :product_ids => map(pair_to_string, pairs),
    :type => :subscribe,
    :channels => [level]
)
url = "wss://ws-feed.pro.coinbase.com"

launch(pairs_dict::Dict, updater::Link; kwargs...) = launch_genaric(config, pairs_dict, updater; kwargs...)

config(pairs::Dict, out_channel::Destination; kwargs...) = config(pairs[:coinbasepro]...;  out_channel=out_channel)
config(pairs...; out_channel = out_channel::Destination) = WebSocketConfig(
    feed = symbol,
    subscribe_msg = subscribe_msg(pairs; level = :ticker),
    url = "wss://ws-feed.pro.coinbase.com",
    out_channel = out_channel,
    setup_func = setup_handler
)

#=
function handle_l2update_book!(jd::Dict, book_ind::Dict; book_size::Int64=10)
    pair = get_pair(jd["product_id"])
    timestamp = parse_time(jd["time"])
    book_old = book_ind[pair] |> last
    book = Book(timestamp, book_old) # creates new book
    function inlinefunc(vec::Vector)
        ss, pp, vv = vec
        p = parse(Float64,pp)
        v = parse(Float64,vv)
        if ss == "buy"
            lowest_buy = book.bids.keys[end]
            if v == 0
                pop!(book.bids, p, false)
            elseif p >= lowest_buy
                book.bids[p] = v
                sort!(book.bids, rev=true)
                if length(book.bids) > book_size
                    pop!(book.bids, lowest_buy, false)
                end
            end
        else
            @assert ss == "sell"
            highest_sell = book.asks.keys[end]
            if v == 0
                pop!(book.asks, p, false)
            elseif p <= highest_sell
                book.asks[p] = v
                sort!(book.asks)
                if length(book.asks) > book_size
                    pop!(book.asks, highest_sell, false)
                end
            end
        end
    end
    try
        map(inlinefunc, jd["changes"])
    catch
        @show jd["changes"]
    end
    push!(book_ind[pair], book) # replaces old book
    book
end

proc_task = @task for pmsg in process_chan
    push!(c, pmsg)
    book = handler(pmsg)
    if book isa Book
        bid, bidv = get(collect(book.bids), 1, (0,0))
        ask, askv = get(collect(book.asks), 1, (0,0))
        out = OrderedDict(
            :type => :ticker,
            :feed => :coinbasepro,
            :pair => (:BTC, :USD),
            :timestamp => string(book.timestamp),
            :time_received => string(book.time_received),
            :bid => bid,
            :bid_volume => bidv,
            :ask => ask,
            :ask_volume => askv
        )
        push!(d, out)
        send(ws_socket, out |> JSON.json)
    end
end

wst = @task WebSockets.open(url) do wc
    writeguarded(wc, subscribe_msg |> JSON.json)
    stillopen = true
    while stillopen
        msg, stillopen = readguarded(wc)
        if stillopen
            pmsg = msg |> String |> JSON.parse
            put!(process_chan, pmsg)
        else
            break
        end
    end
    println("something happened!!!")
end
=#
end # module
