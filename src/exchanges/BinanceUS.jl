module BinanceUS

using WebSockets, JSON, Actors
using ArbUtils.OrderUtils, Currencies
using ..SocketBase

const symbol = :binanceus

pair_to_string(pair) = map(Symbol, pair) |> pair_to_string
pair_to_string(pair::Tuple{Symbol, Symbol}) = string(pair[1],pair[2]) |> lowercase

function subscribe_msg(pairs)
    pairs_params = map(pairs) do p
        pair_to_string(p) * "@bookTicker"
    end
    Dict(
        :method => :SUBSCRIBE,
        :params => collect(pairs_params),
        :id => 1
    )
end

function url_maker(pairs)
    base_url = "wss://stream.binance.us:9443"
    pairs_params = map(pairs) do p
        pair_to_string(p) * "@bookTicker"
    end
    url = base_url * "/stream?streams="
    for p in pairs_params
        url = url*p*"/"
    end
    url[1:end-1]
end

launch(pairs_dict::Dict, updater::Link; kwargs...) = launch_genaric(config, pairs_dict, updater; kwargs...)

config(pairs::Dict, out_channel::Destination; kwargs...) = config(pairs[:binanceus]...;  out_channel=out_channel)
config(pairs...; out_channel::Destination) = WebSocketConfig(
    feed = :binanceus,
    subscribe_msg = subscribe_msg(pairs),
    url = url_maker(pairs),
    out_channel = out_channel,
    setup_func = presetup_handler(pairs)
)

function presetup_handler(pairs)

    # need closure for pairs comparison

    pairs_list_raw = map(pair_to_string, pairs)

    pair_dict = Dict(uppercase(s)=>p for (s,p) in zip(pairs_list_raw, pairs))
    pair_lookup(s) = pair_dict[s]

    function setup_handler(chan::Destination)

        # closure here can handle state if needed

        handler(resp) = begin
            @warn "This is a default response for BinanceUS; something isn't plumbed right"
            @show resp
        end
        function handler(resp::Dict)
            try
                if "stream" in keys(resp)
                    tick = handle_ticker(resp["data"], pair_lookup)
                    put!(chan, tick)
                elseif "result" in keys(resp)
                    @info "This is a subscription message"
                    @show resp
                else
                    @warn "Not expecting this message"
                    @show resp
                end
            catch x
                @warn x
                @show resp
            end
        end
        handler
    end
end

function handle_ticker(dict::Dict, pair_lookup::Function)
    tn = time()
    pair = dict["s"] |> pair_lookup
    b,bv,a,av = map(("b", "B", "a", "A")) do k
        parse(Float64, dict[k])
    end
    Tick(
        symbol,
        pair,
        b,
        bv,
        a,
        av,
        tn,
        tn,
        [:bid, :ask]
    )
end

end # end module
