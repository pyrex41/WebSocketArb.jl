module SocketBase

using Dates, JSON, OrderedCollections, WebSockets, Actors
using ArbUtils.OrderUtils
using Currencies

import Actors: spawn, send, exit!, connect, exec

import Base.string
import Base.float
import Base.put!

export PrecisionDateTime, string, opensocket, float, WebSocketConfig, parse_time,
    get_pair, debugsocket, Destination, UpdateBundle, launch_genaric, exit!, tryexit!

function get_pair(str::String, spl::String="-")
    map(split(str, spl)) do c
        c |> uppercase |> Symbol
    end |> Tuple
end

struct PrecisionDateTime
    date::Dates.Date
    time::Dates.Time
end
now_precise() = now(UTC) |> string |> parse_time
string(pdt::PrecisionDateTime) = string(pdt.date, "T", pdt.time,"Z")
float(pdt::PrecisionDateTime) = (pdt.date |> DateTime |> datetime2unix) + round(pdt.time.instant.value*10^-9, digits=9)
put!(link::Link, msg) = send(link, msg)

Destination = Union{Link, Channel{Tick}}

function parse_time(str_raw::String)
    str = replace(str_raw, "Z"=>"")
    sd = split(str, "T")
    dt = Date(sd[1])
    sdt = split(sd[2], ".")
    ss = split(sdt[1], ":")
    l2 = length(sdt[2])
    for i=0:div(l2, 3)-1
        i1 = i*3 + 1
        i2 = min(i1+2, l2)
        push!(ss, sdt[2][i1:i2])
    end
    tm = Time(map(x->parse(Int64, x), vcat(ss))...)
    PrecisionDateTime(dt, tm)
end

struct WebSocketConfig
    feed::Symbol
    subscribe_msg::Dict
    url::String
    handler::Link
    out_process::Destination
end
function WebSocketConfig(;feed::Symbol, subscribe_msg::Dict, url::String, setup_func::Function, out_channel::Union{Link, Channel{Tick}}, kwargs...)
    handler = setup_func(out_channel)
    handler_actor = spawn(handler)
    WebSocketConfig(
        feed,
        subscribe_msg,
        url,
        handler_actor,
        out_channel
    )
end

struct UpdateBundle
    config::WebSocketConfig
    handler::Link
    wstask::Link
    updater::Link
end
function launch_genaric(config::Function, pairs_dict::Dict, outlink::Link; start = true, kwargs...)
    trapExit(outlink)
    wsc = config(pairs_dict, outlink; kwargs...)
    wst = spawn(openwebsocket(wsc; start = start))
    exec(wsc.handler, connect, wst)
    exec(wsc.handler, connect, outlink)
    UpdateBundle(
        wsc,
        wsc.handler,
        wst,
        outlink
    )
end
exit!(ubp::UpdateBundle) = exit!(ubp.wstask)
tryexit!(x) = try
    exit!(x)
catch w
    @warn w
end


function openwebsocket(wsc::WebSocketConfig; start=true)
    wst = @task WebSockets.open(wsc.url) do wc
        writeguarded(wc, wsc.subscribe_msg |> JSON.json)
        stillopen = true
        msg = :init
        while stillopen
            msg, stillopen = readguarded(wc)
            if stillopen
                pmsg = msg |> String |> JSON.parse
                send(wsc.handler, pmsg)
            end
        end
        last_msg = String(msg)
        @warn "Websocket closed"
        @info "Last message is:"
        @show last_msg
        @show stillopen
    end
    if start
        schedule(wst)
    end
    wst
end

function debugsocket(subscribe_msg, url, actor)
    WebSockets.open(url) do wc
        writeguarded(wc, subscribe_msg |> JSON.json)
        stillopen = true
        msg = :init
        for i=1:10
            msg, stillopen = readguarded(wc)
            if stillopen
                pmsg = msg |> String |> JSON.parse
                send(actor, pmsg)
            end
        end
        last_msg = String(msg)
        @warn "Websocket closed"
        @info "Last message is:"
        @show last_msg
        @show stillopen
    end
end

struct Book{T<:Currency, U<:Currency}
    timestamp::PrecisionDateTime
    time_received::PrecisionDateTime
    bids::OrderedDict{Float64,Float64}
    asks::OrderedDict{Float64,Float64}
end
Book(T::Type{<:Currency}, U::Type{<:Currency}) = Book{T,U}(now_precise(), now_precise(), OrderedDict{Float64,Float64}(), OrderedDict{Float64,Float64}())
Book(timestamp::PrecisionDateTime, book::Book{T,U}) where {T <: Currency, U <: Currency} = Book{T,U}(timestamp, now_precise(), book.bids, book.asks)


end #end module
